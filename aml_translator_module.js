
var AMLTranslator = {
  amlKnowElements: ["^%","^!%","^~","^!~"],
  amlKnowElementsTraslations: ["<strong>","</strong>","<em>","</em>"],

  amlKnowOpeningElements: ["^%","^~","^#"],
  amlKnowClosingElements: ["^!%","^!~","^!#"],


  translate : function(amlString) {
    var self = this;
    amlString = self.validateAMLString(amlString);
    this.amlKnowElements.forEach(function(val,index){
      var positions = self.getPositionOfAMLElements(val,amlString);
      if (positions.length >= 1) {
        for (var i = 1; i <= positions.length; i++) {
          amlString = amlString.replace(val, self.amlKnowElementsTraslations[index]);
        }
      }
    })
    return amlString;
  },
  getPositionOfAMLElements : function(amlKnowWord, amlString) {
     var amlKnowWordLength = amlKnowWord.length;
     if (amlKnowWordLength == 0) {
         return [];
     }
     var startIndex = 0, index, positions = [];
     while ((index = amlString.indexOf(amlKnowWord, startIndex)) > -1) {
         positions.push(index);
         startIndex = index + amlKnowWordLength;
     }
     return positions;
 },
 validateAMLString : function(amlString){
   var self = this;
   self.amlKnowClosingElements.forEach(function(amlKnowClosingElement,index){
     elementsToSearch = self.getOpeningElementsOfPossibleMissedClosedElements(index);
     elementsToSearch.forEach(function(elementToSeach){
       var positiosOfClosingElements = self.getPositionOfAMLElements(amlKnowClosingElement,amlString)
       var elementThatMayBeClosedFirst = self.amlKnowOpeningElements.indexOf(elementToSeach)
       if (positiosOfClosingElements.length > 1) {
         positiosOfClosingElements = positiosOfClosingElements.pop()
       }
       var pos = self.getPositionOfAMLElements(elementToSeach,amlString)
       if (pos.length !=0 && positiosOfClosingElements.length != 0) {
         if ( self.checkIfMissingCloseElementBeforeCloseNewOne(elementThatMayBeClosedFirst, amlString, pos, positiosOfClosingElements) ) {
           var start = self.cutString(amlString,0,positiosOfClosingElements[0]);
           var end = self.cutString(amlString,positiosOfClosingElements[0]+amlKnowClosingElement.length ,amlString.lengt);
           amlString = self.repairUnclosedString(start, self.amlKnowClosingElements[elementThatMayBeClosedFirst], amlKnowClosingElement, self.amlKnowOpeningElements[elementThatMayBeClosedFirst], end)
         }
       }
     })

   })
   return amlString;
 },
 checkIfMissingCloseElementBeforeCloseNewOne:function(elementThatMayBeClosedFirst, amlString, start, end){
   var self = this;
   return (self.getPositionOfAMLElements( self.amlKnowClosingElements[elementThatMayBeClosedFirst],self.cutString(amlString,start,end) )).length == 0
 },
 getOpeningElementsOfPossibleMissedClosedElements : function(index){
   var self = this;
    return self.amlKnowOpeningElements.filter(function(ele){
       return ele != self.amlKnowOpeningElements[index];
     });
 },
 repairUnclosedString : function(startOfString, amlMissingCloseElement, element, amlMissingOpenElement, endOfString ){
   return startOfString + amlMissingCloseElement + element + amlMissingOpenElement + endOfString;;
 },
 cutString : function(str,start,end){
   return str.slice(start,end)
 }
};

// Make translator available via “require” in Node.js
if (module.exports) {
  module.exports = AMLTranslator
}
