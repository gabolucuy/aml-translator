# AML: Alien Markup Language

## Introduction

In the year 2018, radiation from a solar flare mutates the Hubble Space Telescope allowing us to receive data being transmitted on the extra-terrestrial Internet. After observing transmissions for some time, we have established that our cosmic neighbors use a markup language that is notably different from HTML. In particular, it is not strictly hierarchical, an element may extend past the closing of the element in which it was opened. We want to build a javascript component that will translate AML into HTML.

## About AML

Like HTML, AML has matched pairs of opening and closing elements, indicating that the text between opening and closing elements should have a particular text effect applied. All AML markup elements begin with a caret (^), optionally are followed by an exclamation point indicating it is a closing element, and are followed by a single character effect type indicator. For a string to be valid AML, all elements that are opened must be closed before the end of the document, and bare caret (^) characters (not part of a markup element) are not permitted.

Unlike HTML, AML elements that are opened within a different AML effect do not need to be enclosed by that effect.

## The Task

The task is to write a javascript file that defines a global object with a callable method named translate.

translate should take a string of valid AML as its sole argument, and return a valid HTML fragment with no wrapping elements. It is not necessary to validate the provided AML, but valid AML should result valid HTML fragments. So translating the above example into HTML with the desired text effects applied looks like the following:

```
var myHTML = AMLTranslator.translate(“Greetings ^%from ^~Glornix^!% Beta-Nine^!~.”)
```

and myHTML would contain the string:

```
Greetings <strong>from <em>Glornix</em></strong><em> Beta-Nine</em>.
```

## Important considerations

To execute the AML tester file after cloning the project run the following command:

```
npm install mock-browser
```
And then:

```
node ./aml_tester.js ./aml_translator_module.js
```
